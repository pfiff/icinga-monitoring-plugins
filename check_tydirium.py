#!/usr/bin/python3

import sys, json, requests;

url = "http://tydirium.web.itv.local/status"

response = requests.get(url, verify=True)

if (response.ok):
	data = json.loads(response.text)
	if data['config'] and data['versionConflict']=="ok":
		print ("OK | currentUserToken=" + data['currentUserToken']['count']+";;; currentNullUserToken="+data['currentNullUserToken']['count'])
		exit(0)
	else:
		print ("CRITICAL: Config missing or Version Conflict")
		exit(2)
else:
	print ("CRITICAL | Could not fetch status")
	exit(2)

