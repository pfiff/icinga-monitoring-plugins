#!/bin/bash


SPOOLPATH=/opt/otrs/var/spool

MAILCOUNT="$(ls $SPOOLPATH | wc -l)"

if [ $MAILCOUNT = 0 ];
then
	echo "OTRS SPOOL OK: $MAILCOUNT E-Mails to be processed | SPOOL=$MAILCOUNT;;;;"
	exit 0
else
	echo "OTRS SPOOL CRITICAL: $MAILCOUNT E-Mails to be processed | SPOOL=$MAILCOUNT;;;;"
	exit 1
fi

