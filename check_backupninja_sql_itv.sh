#!/bin/bash

BACKUPNAME=$1
BACKUPMINSIZE=$2
BACKUPPATH="/mnt/backup_remote/"

TIME="$(date +%s)"
CURRENTDATE=$(date +%Y-%m-%d)
BFTIME=$(date -d "$CURRENTDATE 21:00:00" +%s)

if [ $TIME -gt $BFTIME ];
then
	DATE="$(date +%Y-%m-%d)"
else
	DATE="$(date +%Y-%m-%d -d '1 days ago')"
fi

FILE=$BACKUPPATH$DATE"_"$BACKUPNAME.sql.gz

if [ -e $FILE ];
then
	FILESIZE=`du -k "$FILE" | cut -f1`
	if [ $FILESIZE -gt $BACKUPMINSIZE ];
	then
		echo "BACKUP OK: $1 | $1=$FILESIZE;$2;$2;;"
		exit 0
	else
		echo "BACKUP CRITICAL: Backup too small for $1: $FILESIZE < $BACKUPMINSIZE | $1=$FILESIZE;$2;$2;;"
		exit 1
	fi
else
	echo "BACKUP CRITICAL: No backup found for $1| $1=0;$2;$2;;"
	exit 2
fi
