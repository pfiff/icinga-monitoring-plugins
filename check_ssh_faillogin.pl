#!/usr/bin/perl -w

# Author: Shagin Enose
# Date: 18-Dec-2010
# Type: Naigos Plugin
# Description: Checks ssh failed login attempts


use strict;
use English;
use Getopt::Long;
use File::stat;
use vars qw($PROGNAME);
use lib "/usr/lib/nagios/plugins";
use utils qw (%ERRORS &print_revision &support);

sub print_help ();
sub print_usage ();

my ($opt_c, $opt_w, $opt_h, $opt_V);
my ($result, $message, $resultvalue);
my $failedPasswordCount;
my $failedCertificateCount;
my $failedInvalidUserCount;
my $failedSum;
my $position;
my $CMDFILE;
my $TIME;

chomp(my $HOSTNAME=`hostname --fqdn`);

$CMDFILE="/var/run/icinga2/cmd/icinga2.cmd";

$PROGNAME="check_ssh_faillogin";

$opt_w = 5;
$opt_c = 10;

Getopt::Long::Configure('bundling');
GetOptions(
	"V"   => \$opt_V, "version"	=> \$opt_V,
	"h"   => \$opt_h, "help"	=> \$opt_h,
	"w=i" => \$opt_w, "warning=i" => \$opt_w,
	"c=i" => \$opt_c, "critical=i" => \$opt_c);

if ($opt_V) {
	print_revision($PROGNAME, '@NP_VERSION@');
	exit $ERRORS{'OK'};
}

if ($opt_h) 
{
	print_help();
	exit $ERRORS{'OK'};
}

$result = 'OK';

$failedPasswordCount=0;
$failedCertificateCount=0;
$failedInvalidUserCount=0;
$failedSum=0;
$position=0;
my $statfile ='/usr/lib/nagios/plugins/var/check_ssh_faillogin.dat';
if(-e $statfile)
 {
        open(STAT_HAND, "<$statfile") or die "an error occured: $!";
        while(<STAT_HAND>)
        {
                $position = $_;
        }
        close(STAT_HAND);
 }
open (FILEHANDLE, "</var/log/auth.log") or die "an error occured: $!";
seek FILEHANDLE,$position , 0;
while(<FILEHANDLE>)
{
        if(index($_,"Failed password for",0)!=-1)
        {
                $failedPasswordCount++;
        }
	if(index($_,"error: Certificate does not contain an authorized principal",0)!=-1)
        {
                $failedCertificateCount++;
        }
	if(index($_,"Invalid user",0)!=-1)
        {
                $failedCertificateCount++;
        }

$position = tell FILEHANDLE;
}
close(FILEHANDLE) or die "an error occured while trying to close the file: $!";

open(STAT_HAND, "+>$statfile") or die "an error occured: $!";
print STAT_HAND "$position";
close(STAT_HAND);

$failedSum = $failedPasswordCount+$failedCertificateCount+$failedInvalidUserCount;

if ($failedSum > $opt_c) {
	$resultvalue = 2;
	$result = 'CRITICAL';
} elsif ($failedSum > $opt_w) {
	$resultvalue = 1;
	$result = 'WARNING';
} else {
	$resultvalue = 0;
	$result = 'OK';
}
$TIME=time;



print "$TIME PROCESS_SERVICE_CHECK_RESULT;$HOSTNAME;SSH Failed Logins;$resultvalue;LOGIN ATTEMPTS $result: $failedSum failed login attempts | Total=$failedSum;$opt_w;$opt_c; Failed_Password_Attempts=$failedPasswordCount;;; Failed_Certificate_Attempts=$failedCertificateCount;;; Invalid_User_Attempts=$failedInvalidUserCount;;;\n";
exit $ERRORS{$result};

sub print_usage () {
	print "Usage:\n";
	print "  $PROGNAME [-w <count>] [-c <count>] \n";
	print "  $PROGNAME [-h | --help]\n";
	print "  $PROGNAME [-V | --version]\n";
}

sub print_help () {
	print_revision($PROGNAME, '@NP_VERSION@');
	print "Copyright (c) 2010 Shagin Enose\n\n";
	print_usage();
	print "\n";
	print "  <count>  count must be a positive number(default: warn 5, crit 10)\n";
	print "\n";
	support();
}


