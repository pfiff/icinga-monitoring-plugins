#!/usr/bin/python
import re, os, sys, argparse, getopt, time

sys_path = "/sys/bus/w1/devices/"

#global temp1

def main(argv):
    try:
        opts, args = getopt.getopt(argv,"hs:c:w:C:W:")
    except getopt.GetOpt.Error:
        print 'check_1-wiretmep.py -s SensorID -w warnHIGH -c CritHIGH'
        sys.exit(2)
    if not opts:
	help()
    for opt, arg in opts:
        if opt == '-h':
            help()
	elif opt =="-s":
            sensor = arg
        elif opt == "-c":
            crit_high = float(arg)
        elif opt == "-w":
            warn_high = float(arg)
        elif opt == "-C":
            crit_low = float(arg)
        elif opt == "-W":
            warn_low = float(arg)
    value = "U"
    sen_path = sys_path + sensor + "/w1_slave"
    #state1 = state(sensor, crit_high, warn_high, crit_low, warn_low)
    temp1 = float(read_sensor(sen_path))
    ausgabe=state(temp1, crit_high, warn_high, crit_low, warn_low)

def help():
    print "Hier kommt die Hilfe hin - MACHEN!!!!!!"
    print "check_1-wiretmep.py -s SENSORID -c CRITLOW -w WARNHIGH -C CRITLOW -W WARNLOW"
    sys.exit()

def missing(s):
    print "Missing nessesary value: %s." % s
    print
    help()
    sys.exit()

def read_sensor(path):
  value = "U"
  try:
    f = open(path, "r")
    line = f.readline()
    if re.match(r"([0-9a-f]{2} ){9}: crc=[0-9a-f]{2} YES", line):
      line = f.readline()
      m = re.match(r"([0-9a-f]{2} ){9}t=([+-]?[0-9]+)", line)
      if m:
        value = str(float(m.group(2)) / 1000.0)
    f.close()
  except (IOError), e:
    print time.strftime("%x %X"), "Error reading", path, ": ", e
    sys.exit(3)
  return value

################

def state(temp1, crit_high, warn_high, crit_low, warn_low):
    if temp1 > crit_high:
	print "Temp CRITICAL High "+str(temp1)+" | Temp="+str(temp1)+";;;"
	sys.exit(2)
    elif temp1 < crit_low:
	print "Temp CRITICAL Low "+str(temp1)+" | Temp="+str(temp1)+";;;"
	sys.exit(2)
    elif temp1 > warn_high:
        print "Temp WARNING High "+str(temp1)+" | Temp="+str(temp1)+";;;"
	sys.exit(1)
    elif temp1 < warn_low:
        print "Temp WARNING Low "+str(temp1)+" | Temp="+str(temp1)+";;;"
	sys.exit(1)
    else:
	print "Temp OK "+str(temp1)+" | Temp="+str(temp1)+";;;"
	sys.exit(0)

if __name__ == "__main__":
    main(sys.argv[1:])