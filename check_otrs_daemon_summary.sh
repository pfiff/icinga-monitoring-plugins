#!/bin/bash

DAEMONNAME=$1

DAEMONLINE="$(/opt/otrs/bin/otrs.Console.pl Maint::Daemon::Summary | grep $DAEMONNAME)"
DAEMONLASTSTATE="$(echo $DAEMONLINE | awk '{print $7;}')"
TIMESTAMPS=$(echo "$DAEMONLINE" | grep -Eo '[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2} [[:digit:]]{2}:[[:digit:]]{2}:[[:digit:]]{2}')
IFS=$'\n'
GLOBIGNORE='*'
EXECUTIONTIMES=($(echo "$TIMESTAMPS"))
LASTEXECUTIONTIME="${EXECUTIONTIMES[0]}"
NEXTEXECUTIONTIME="${EXECUTIONTIMES[1]}"

CURRENTUTIME=$(date +%s)
NEXTEXECUTIONUTIME=$(date -d "$NEXTEXECUTIONTIME" +%s)

if [ $CURRENTUTIME -gt $NEXTEXECUTIONUTIME ];
then
        echo "OTRS Daemon $DAEMONNAME CRITICAL: Overdue"
        exit 2
fi

if [ "$DAEMONLASTSTATE" == "Success" ];
then
        echo "OTRS Daemon $DAEMONNAME OK"
        exit 0
else
        echo "OTRS Daemon $DAEMONNAME CRITICAL"
        exit 1
fi

